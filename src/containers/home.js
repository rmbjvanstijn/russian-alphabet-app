import React, { Component } from 'react';

//Material ui
import Typography from '@material-ui/core/Typography';

class Home extends Component {
    render() {
        return (
            <Typography variant="display3" gutterBottom>
                Home
            </Typography>
        );
    }
}

export default Home;