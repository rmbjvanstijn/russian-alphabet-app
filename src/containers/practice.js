import React, { Component } from 'react';
import Question from "../components/question";
import Form from "../components/form";
import Grid from '@material-ui/core/Grid';

class PracticeContainer extends Component {
    render() {
        const { props } = this;
        const { startQuestion, endQuestion } = props;
        return (
            <div className="container">
                <Grid container spacing={24}>
                    <Grid item xs={12} sm={6}>
                        <Question
                            startQuestion={startQuestion}
                            endQuestion={endQuestion}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <Form
                            startQuestion={startQuestion}
                            endQuestion={endQuestion}
                        />
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default PracticeContainer;