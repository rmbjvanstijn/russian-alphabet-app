import React from 'react';

//Material ui
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

//Json
import answerJson from './../answers.json';

function AnwserContainer() {
    const answerFile = JSON.parse(JSON.stringify(answerJson));

    return (
        <Paper>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Russian</TableCell>
                        <TableCell>Dutch</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {answerFile.map(answer => {
                        return (
                            <TableRow key={answer.id}>
                                <TableCell>{answer.russian}</TableCell>
                                <TableCell>{answer.dutch}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </Paper>
    );
}

export default AnwserContainer;