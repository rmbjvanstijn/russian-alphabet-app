import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";
import store from "./store/index";
// import Routes from "./routes/";
import Header from "./components/header";
import PracticeContainer from './containers/practice';
import AnwserContainer from './containers/anwser';
import Home from './containers/home';


render(
    <Provider store={store}>
        <Router >
            <div>
                <Header />
                <Route exact path="/" component={Home} />
                <Switch>
                    <Route exact path="/oefenen" render={(props) => (<PracticeContainer startQuestion="0" endQuestion="32" {...props} />)} />
                    <Route exact path="/oefenen/0-5" render={(props) => (<PracticeContainer startQuestion="0" endQuestion="5" {...props} />)} />
                    <Route exact path="/oefenen/6-10" render={(props) => (<PracticeContainer startQuestion="5" endQuestion="10" {...props} />)} />
                    <Route exact path="/oefenen/11-15" render={(props) => (<PracticeContainer startQuestion="10" endQuestion="15" {...props} />)} />
                    <Route exact path="/oefenen/16-20" render={(props) => (<PracticeContainer startQuestion="15" endQuestion="20" {...props} />)} />
                    <Route exact path="/oefenen/21-25" render={(props) => (<PracticeContainer startQuestion="20" endQuestion="25" {...props} />)} />
                    <Route exact path="/oefenen/26-30" render={(props) => (<PracticeContainer startQuestion="25" endQuestion="30" {...props} />)} />
                    <Route exact path="/oefenen/31-33" render={(props) => (<PracticeContainer startQuestion="30" endQuestion="33" {...props} />)} />
                </Switch>
                <Route path="/antwoorden" component={AnwserContainer} />

            </div>
        </Router>
    </Provider >,
    document.getElementById("app")
);