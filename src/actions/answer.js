import { CHECK_ANSWER, NEW_ANSWER } from '../constants/endpoints';
import answerJson from './../answers.json';

export function checkAnswer(givenAnswerIsCorrect) {
    return dispatch => {
        dispatch({
            type: CHECK_ANSWER,
            givenAnswerIsCorrect
        });
    };
};

export function newAnswer(startQuestion, endQuestion, arrayNumber) {
    const answerFile = JSON.parse(JSON.stringify(answerJson));
    const anwserFilePractice = answerFile.slice(parseInt(startQuestion, 10), parseInt(endQuestion, 10));
    const answerFileLength = anwserFilePractice.length;
    let randomArrayNumer = 0;
    do {
        randomArrayNumer = Math.floor(Math.random() * Math.floor(answerFileLength));
    } while (randomArrayNumer === arrayNumber);
    const newAnswer = anwserFilePractice[randomArrayNumer];

    return dispatch => {
        dispatch({
            type: NEW_ANSWER,
            newAnswer,
            randomArrayNumer
        });
    };
}

