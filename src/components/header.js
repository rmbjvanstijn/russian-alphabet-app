import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//Material-ui
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    list: {
        width: 250,
    }
};

class Header extends Component {
    state = {
        drawer: false,
    };

    toggleDrawer = (isOpen) => () => {
        this.setState({
            drawer: isOpen,
        });
    };

    render() {
        const sideList = (
            <div style={styles.list}>
                <List >
                    <ListItem component={Link} to="/">
                        <ListItemText primary="Home" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen">
                        <ListItemText primary="Practice all" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/0-5">
                        <ListItemText secondary="Practice 0-5" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/6-10">
                        <ListItemText secondary="Practice 6-10" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/11-15">
                        <ListItemText secondary="Practice 11-15" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/16-20">
                        <ListItemText secondary="Practice 16-20" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/21-25">
                        <ListItemText secondary="Practice 21-25" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/26-30">
                        <ListItemText secondary="Practice 26-30" />
                    </ListItem>
                    <ListItem component={Link} to="/oefenen/31-33">
                        <ListItemText secondary="Practice 31-33" />
                    </ListItem>
                    <ListItem component={Link} to="/antwoorden">
                        <ListItemText primary="Answers" />
                    </ListItem>
                </List>
            </div>
        );

        return (
            <React.Fragment>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton style={styles.menuButton} color="inherit" aria-label="Menu">
                            <MenuIcon onClick={this.toggleDrawer(true)} />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <SwipeableDrawer
                    open={this.state.drawer}
                    onClose={this.toggleDrawer(false)}
                    onOpen={this.toggleDrawer(true)}
                >
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.toggleDrawer(false)}
                        onKeyDown={this.toggleDrawer(false)}
                    >
                        {sideList}
                    </div>
                </SwipeableDrawer>
            </React.Fragment>
        )
    }
}

export default Header;