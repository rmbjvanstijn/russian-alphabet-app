import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';

//Components
import AnswerDialog from './answerDialog';

//Actions
import * as ArticleActions from './../actions/answer';

//Material ui
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DoneIcon from '@material-ui/icons/Done';
import ClearIcon from '@material-ui/icons/Clear';
import Grid from '@material-ui/core/Grid';

class ConnectedForm extends Component {
    constructor() {
        super();

        this.state = {
            open: false,
            title: "",
            opacityGreen: '0.2',
            opacityRed: '0.2'
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { state, props } = this;
        const { actions, newAnswer, randomArrayNumer, startQuestion, endQuestion } = props;
        const { title } = state;
        const answerIsCorrect = title.toLowerCase === newAnswer.dutch.toLowerCase;

        if (answerIsCorrect) {
            this.setState({
                title: '',
                opacityGreen: '0.8',
                opacityRed: '0.2'
            })
            actions.checkAnswer(answerIsCorrect);
            actions.newAnswer(startQuestion, endQuestion, randomArrayNumer);
        }
        else {
            this.setState({
                opacityGreen: '0.2',
                opacityRed: '0.8'
            })
            actions.checkAnswer(answerIsCorrect);
        }

        setTimeout(() => {
            this.setState({
                opacityGreen: '0.2',
                opacityRed: '0.2'
            })
        }, 500)
    }

    handleClickOpen = () => {
        this.setState({
            open: true,
        });
    };

    handleClose = () => {
        this.setState({
            open: false
        });
    };

    render() {
        const { props, state } = this;
        const { newAnswer } = props;
        const { opacityGreen, opacityRed } = state;

        return (
            <div>
                <div className="o-box">
                    <Grid container spacing={24}>
                        <Grid item xs={6}>
                            <div className="icon-container-green text-center" style={{ "opacity": opacityGreen }}>
                                <DoneIcon style={{ color: "white" }} />
                            </div>
                        </Grid>
                        <Grid item xs={6}>
                            <div className="icon-container-red text-center" style={{ "opacity": opacityRed }}>
                                <ClearIcon style={{ color: "white" }} />
                            </div>
                        </Grid>
                    </Grid>
                </div>
                <form onSubmit={this.handleSubmit}>
                    <Grid container spacing={24} >
                        <Grid item xs={8}>
                            <div className="form-group">
                                <TextField
                                    id="title"
                                    label="title"
                                    value={this.state.title}
                                    onChange={this.handleChange}
                                    margin="normal"
                                    autoComplete="off"
                                />
                            </div>
                        </Grid>
                        <Grid item xs={4} className="center-div">
                            <Button type="submit" variant="contained" color="primary">
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </form >
                <div className="text-center o-box">
                    <Button onClick={this.handleClickOpen}>Give me a hint!</Button>
                    <AnswerDialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        newAnswer={newAnswer}
                    />
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, ArticleActions), dispatch),
    };
}

const mapStateToProps = state => ({
    newAnswer: state.answer.newAnswer,
    randomArrayNumer: state.answer.randomArrayNumer
});

const Form = connect(mapStateToProps, mapDispatchToProps)(ConnectedForm);

export default Form;