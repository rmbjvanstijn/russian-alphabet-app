import React, { Component } from 'react';

//Material ui
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Typography from '@material-ui/core/Typography';

class AnswerDialog extends Component {
    handleClose = () => {
        this.props.onClose(this.props.selectedValue);
    };

    handleListItemClick = value => {
        this.props.onClose(value);
    };

    render() {
        const { newAnswer, ...other } = this.props;

        return (
            <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}>
                <DialogTitle id="simple-dialog-title">Anwser</DialogTitle>
                <div className="text-center">
                    <Typography variant="display2" gutterBottom>
                        {newAnswer.dutch}
                    </Typography>
                </div>
            </Dialog>
        );
    }
}

export default AnswerDialog;