import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";

//Actions
import * as AnwserActions from './../actions/answer';

//Material ui
import Typography from '@material-ui/core/Typography';

class ListItems extends Component {
    constructor(props) {
        super();
    }

    componentDidMount() {
        const { props } = this;
        const { actions, startQuestion, endQuestion } = props;
        actions.newAnswer(startQuestion, endQuestion);
    }

    render() {
        const { props } = this;
        const { newAnswer } = props;
        const question = newAnswer !== undefined ? newAnswer.russian : "";

        return (
            <div className="text-center">
                <Typography variant="display4" gutterBottom>
                    {question}
                </Typography>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Object.assign({}, AnwserActions), dispatch),
    };
}

const mapStateToProps = state => ({
    newAnswer: state.answer.newAnswer
});

const list = connect(mapStateToProps, mapDispatchToProps)(ListItems);

export default list;