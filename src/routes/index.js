import React from 'react';
import { Switch, BrowserRouter as Route } from 'react-router-dom'

//Routes components
import ListContainer from './../containers/list';
import Home from './../containers/home';

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/list" component={ListContainer} />
        </Switch>
    );
}

export default Routes;
