import { CHECK_ANSWER, NEW_ANSWER } from "../constants/endpoints";

const initialState = {
    newAnswer: [],
    answer: false
}

const answerReducer = (state = initialState, action) => {
    const { type, newAnswer, randomArrayNumer, givenAnswerIsCorrect } = action;
    switch (type) {
        case CHECK_ANSWER: {
            return {
                ...state,
                givenAnswerIsCorrect
            };
        }
        case NEW_ANSWER: {
            return {
                ...state,
                newAnswer,
                randomArrayNumer
            };
        }

        default:
            return state;
    }
};

export default answerReducer;